<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPivotTableForGeneontologiesProteins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('pcosproteins_geneontologies', function(Blueprint $table) {
            $table->foreign('pcos_id')->references('pcos_id')->on('pcosproteins');
            $table->foreign('go_id')->references('go_id')->on('geneontologies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
