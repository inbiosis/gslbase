<footer class="footer" style="z-index: 3">
  <div class="container">
    <p class="text-muted">
      © 2018, Bioinformatics & Computational Systems Biology Research Group (BCSB) | SuCComBase v1.2 | Last update: 22th May 2020
    </p>
  </div>
</footer>