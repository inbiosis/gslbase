<div class="navbar navbar-fixed-top navbar-bold" data-spy="affix" data-offset-top="1000">
  <div class="container">
    <div class="navbar-header">
      <a href="{{url('/')}}" class="navbar-brand">Home</a>
      <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
      <ul class="nav navbar-nav">
        <li><a href="{{url('/about')}}">About</a></li>
        <li><a href="{{url('/browse')}}">Browse</a></li>
        <li><a href="{{url('/search')}}">Search</a></li>
        <li class="dropdown{!! Request::is('genes*') | Request::is('putatives*') | Request::is('coexpressions*') | Request::is('results*') | Request::is('compounds*') | Request::is('references*') ? ' active' : null !!}">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Datasets
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li{!! Request::is('genes') ? ' class="active"' : null !!}><a href="{{ url('/genes') }}">Genes</a></li>
            <li{!! Request::is('putatives') ? ' class="active"' : null !!}><a href="{{ url('/putatives') }}">Putative Genes</a></li>
            <li{!! Request::is('coexpressions') ? ' class="active"' : null !!}><a href="{{ url('/coexpressions') }}">Potential Genes</a></li>
            <li{!! Request::is('results') && !Request::has('result_type_id') ? ' class="active"' : null !!}><a href="{{ url('/results') }}">SCC Homologs</a></li>
            <li{!! Request::is('results') && request('result_type_id') == 1 ? ' class="active"' : null !!}><a href="{{ url('/results?result_type_id=1') }}">Carica papaya SCC Homologs</a></li>
            <li{!! Request::is('results') && request('result_type_id') == 2 ? ' class="active"' : null !!}><a href="{{ url('/results?result_type_id=2') }}">Brassica rapa SCC Homologs</a></li>
            <li{!! Request::is('results') && request('result_type_id') == 3 ? ' class="active"' : null !!}><a href="{{ url('/results?result_type_id=3') }}">Brassica oleracea SCC Homologs</a></li>
            <li{!! Request::is('compounds') ? ' class="active"' : null !!}><a href="{{ url('/compounds') }}">Compounds</a></li>
            <li{!! Request::is('references') ? ' class="active"' : null !!}><a href="{{ url('/references') }}">Publications</a></li>
          </ul>
        </li>
        <li><a href="{{url('/download')}}">Download</a></li>
        <li><a href="{{url('/help')}}">Help</a></li>
        <li><a href="{{url('/contact')}}">Contact</a></li>
      </ul>
    </div>
   </div>
</div>