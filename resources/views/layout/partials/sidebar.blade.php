<!-- sidebar nav -->
<nav id="sidebar-nav">
    <ul class="nav nav-pills nav-stacked">
        <li><a href="{{url('/help')}}">SuCComBase Manual</a></li>
        <li><a href="{{url('/database')}}">SuCComBase Database</a></li>
        <li><a href="{{url('/datasources')}}">Data Sources</a></li>
        <li><a href="{{url('/glossary')}}">Glossary</a></li>
        <li><a href="{{url('/faqs')}}">FAQs</a></li>
    </ul>
</nav>