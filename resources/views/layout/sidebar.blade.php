<!DOCTYPE html>
<html lang="en">
<head>
	@include('layout.partials.head')
</head>

<body id="app">

	@include('layout.partials.nav')

	@include('layout.partials.header')

	<div id="sidebar" class="col-md-2" style="margin-top: 15px">       

		@include('layout.partials.sidebar')

	</div>

	<div id="content" class="col-md-8">
		@yield('content')

	</div>

	@include('layout.partials.footer')

	@include('layout.partials.footer-scripts')

</body>
</html>