@extends('layout.mainlayout')


@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <div class="m-b-md jumbotron">
        <h2>Potential SCC Gene Description</h2>
      </div>
        <table class="table profile-table">
          <tr>
            <td class="table-info col-md-2">Gene name</td>
            <td class="table-detail col-md-10">{{ $coexpression->gene_name }}</td>
          </tr>
          <tr>
            <td class="table-info">AGI ID</td>
            <td class="table-detail"><a href="http://www.arabidopsis.org/servlets/TairObject?type=locus&name={{ $coexpression->agi_id }}" target="_blank">{{ $coexpression->agi_id }}</a></td>
          </tr>
          <tr>
            <td class="table-info">Gene length</td>
            <td class="table-detail">{{ $coexpression->length }}</td>
          </tr>
          <tr>
            <td class="table-info">Uniprot ID</td>
            <td class="table-detail"><a href="http://www.uniprot.org/uniprot/{{ $coexpression->uniprot }}" target="_blank">{{ $coexpression->uniprot }}</a></td>
          </tr>
          <tr>
            <td class="table-info">Protein Name</td>
            <td class="table-detail">{{ $coexpression->protein_name }}</td>
          </tr>
          <tr>
            <td class="table-info">Synonym</td>
            <td class="table-detail">{{ $coexpression->synonym }}</td>
          </tr>
          <tr>
            <td class="table-info">EC number</td>
            <td class="table-detail"><a href="https://enzyme.expasy.org/EC/{{ $coexpression->ec_number }}" target="_blank">{{ $coexpression->ec_number }}</a></td>      
          </tr>
          <tr>
            <td class="table-info">Entrez Gene</td>
            <td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/gene/{{ $coexpression->entrez_gene }}" target="_blank">{{ $coexpression->entrez_gene}}</a></td>
          </tr>
          <tr>
            <td class="table-info">Refseq mrna</td>
            <td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/nuccore/{{ $coexpression->refseq_mrna }}" target="=_blank">{{ $coexpression->refseq_mrna }}</a></td>
          </tr>
          <tr>
            <td class="table-info">Refseq protein</td>
            <td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/protein/{{ $coexpression->refseq_protein }}" target="_blank">{{ $coexpression->refseq_protein }}</a></td>
          </tr>
          <tr>
            <td class="table-info">Database</td>
            <td class="table-detail">
              @foreach($coexpression->databases as $db)
              {{ $db->database }}<br>
              @endforeach
            </td>
          </tr>
        </table>

        <ul class="nav nav-tabs">
        <li class="active">
          <a data-toggle="tab" href="#geneontologies">
            Gene Ontologies{{ count($coexpression->geneontologies) ? ' ('.count($coexpression->geneontologies).')' : '' }}
          </a>
        </li>
      </ul> 

      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="geneontologies">
          @include('geneontologies.list', ['geneontologies' => $coexpression->geneontologies])
        </div>
      </div>

    </div>
  </div>
</div>
@endsection
