@extends('layout.mainlayout')

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <div class="m-b-md jumbotron">
        <h2>{{ $compound->organism->result_type_name }} Compound Description</h2>
      </div>
        <table class="table profile-table">
          <tr>
            <td class="table-info col-md-2">Knapsack ID</td>
            @if($compound->knapsack_id == 'N/A')
            <td class="table-detail col-md-10">{{ $compound->knapsack_id }}</td>
            @else
            <td class="table-detail col-md-10"><a href="http://kanaya.naist.jp/knapsack_jsp/information.jsp?word={{ $compound->knapsack_id }}" target="_blank">{{ $compound->knapsack_id }}</a></td>
            @endif
          </tr>
          <tr>
            <td class="table-info col-md-2">PubChem ID</td>
            @if($compound->pubchem_id == 'N/A')
            <td class="table-detail col-md-10">{{ $compound->pubchem_id }}</td>
            @else
            <td class="table-detail col-md-10"><a href="https://pubchem.ncbi.nlm.nih.gov/compound/{{ $compound->pubchem_id }}" target="_blank">{{ $compound->pubchem_id }}</a></td>
            @endif
          </tr> 
          <tr>
            <td class="table-info">Compound name</td>
            <td class="table-detail">{{ $compound->compound_name }}</td>
          </tr>
          <tr>
            <td class="table-info">Common Name</td>
            <td class="table-detail">{{ $compound->common_name }}</td>
          </tr>
          <tr>
            <td class="table-info">Formula</td>
            <td class="table-detail">{{ $compound->formula }}</td>
          </tr>
          <tr>
            <td class="table-info">SCC Type</td>
            <td class="table-detail">{{ $compound->GSL_type }}</td>
          </tr>
          <tr>
            <td class="table-info">SCC Function</td>
            <td class="table-detail">{{ $compound->compound_function }}</td>
          </tr>
          <tr>
            <td class="table-info">Chemical Structure</td>
            @if($compound->structure == 'N/A')
            <td class="table-detail">{{ $compound->structure }}</td>
            @else
            <td class="table-detail"><img src="{{ url('assets/chem_structure/'.$compound->structure) }}" class="img_fluid" alt="Responsive image" width="400"></td>
            @endif
          </tr>
        </table>
 

        <ul class="nav nav-tabs">
          <li class="active">
          <a data-toggle="tab" href="#references">
              References{{ count($compound->references) ? ' ('.count($compound->references).')' : '' }}
            </a>
          </li>
        </ul> 

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="references">
            @include('references.list', ['references' => $compound->references])
          </div>
        </div> 
   </div>
  </div>

</div>

@endsection
