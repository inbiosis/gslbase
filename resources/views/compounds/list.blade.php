<div class="row">
  <div class="col-md-12 px-0">
    @if(count($compounds))
      <table class="table table-striped">
      <thead>
        <tr>
          <th>Knapsack ID</th>
          <th>Compound Name</th>
          <th>Common Name</th>
          <th>Formula</th>
          <th>Type</th>
          <th>Description</th>
        </tr>
      </thead>
        @foreach ($compounds as $compound)
        <tr>
          <td>
            <p>{{ $compound->knapsack_id }}</p>
          </td>
          <td>
            <p>{{ $compound->compound_name }}</p>
          </td>
          <td>
            <p>{{ $compound->common_name }}</p>
          </td>
          <td>
            <p>{{ $compound->formula }}</p>
          </td>
          <td>
            <p>{{ $compound->GSL_type}}</p>
          </td>
          <td>
            <p><a href="{{ url('compounds/'.$compound->compound_id) }}" ><button class="btn btn-info">Link</button></a></p>
          </td>
        </tr>
      @endforeach
      </table>
    @else {{-- if no compounds in search --}}
      <div class="text-center">
        <h3>No compounds found.</h3>
      </div>
    @endif
  </div>
</div>
