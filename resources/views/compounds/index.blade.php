@extends('layout.mainlayout')

@section('style')
<style>
    div.DTTT_container {
        margin-left: 1em;
    }
    .dataTables_length {
        float: left !important;
    }
    .dataTables_wrapper .dt-buttons {
        float:right !important;
    }
    .dataTables_filter {
        float: right;
        margin-right: 1em;
    }
</style>
@endsection

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      @if(Request::input('result_type_id')==1)
        <h3 class="jumbotron"><i>Carica papaya</i> SCC Compounds</h3>
      @elseif(Request::input('result_type_id')==2)
        <h3 class="jumbotron"><i>Brassica rapa</i> SCC Compounds</h3>
      @elseif(Request::input('result_type_id')==3)   
        <h3 class="jumbotron"><i>Brassica oleracea</i> SCC Compounds</h3>
      @elseif(Request::input('result_type_id')==4)   
        <h3 class="jumbotron"><i>Arabidopsis thaliana</i> SCC Compounds</h3>
      @else
        <h3 class="jumbotron">SCC Compounds</h3>
      @endif       
      <table id="compounds" class="table table-striped table-hover">
        <thead>
          <tr>
            <th><img border="0" alt="W3Schools" src="/assets/knapsack_logo.gif" width="60"></th>
            <th>Compound Name</th>
            <th>Common Name</th>
            <th>Formula</th>
            <th>Type</th>
            <th>Description</th>
          </tr>
        </thead>
        @foreach ($compounds as $compound)
        <tr>
          <td>
            @if($compound->knapsack_id == 'N/A')
            <p>{{ $compound->knapsack_id }}</p>
            @else
            <p><a href="http://kanaya.naist.jp/knapsack_jsp/information.jsp?word={{ $compound->knapsack_id }}" target="_blank">{{ $compound->knapsack_id }}</a></p>
            @endif
          </td>
          <td>
            <p>{{ $compound->compound_name }}</p>
          </td>
          <td>
            <p>{{ $compound->common_name }}</p>
          </td>
          <td>
            <p>{{ $compound->formula }}</p>
          </td>
          <td>
            <p>{{ $compound->GSL_type}}</p>
          </td>
          <td>
            <p><a href="{{ url('compounds/'.$compound->compound_id) }}" ><img border="0" alt="W3Schools" src="/assets/knapsack_logo.gif" width="60"></a></p>  
          </td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  $(function () {
    $('#compounds').DataTable({
      dom: '<lBf<t>ip>',
      buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5' ],
      order: [ 0, 'asc' ]
    });
  });
</script>

@endsection