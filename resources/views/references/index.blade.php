@extends('layout.mainlayout')

@section('style')
<style>
    div.DTTT_container {
        margin-left: 1em;
    }
    .dataTables_length {
        float: left !important;
    }
    .dataTables_wrapper .dt-buttons {
        float:right !important;
    }
    .dataTables_filter {
        float: right;
        margin-right: 1em;
    }
</style>
@endsection

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <h3 class="jumbotron">Publications</h3>  
      <table id="references" class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Pubmed ID</th>
            <th>Year</th>
            <th>Authors</th>
            <th>Title</th>
            <th>Journal</th>
            <th>Description</th>
          </tr>
        </thead>
        @foreach ($references as $reference)
        <tr>
          <td>
            @if($reference->pubmed_id == 'N/A')
              <p>{{ $reference->pubmed_id }}</p>
            @else
              <p><a href="https://www.ncbi.nlm.nih.gov/pubmed/?term={{ $reference->pubmed_id }}" target="_blank">{{ $reference->pubmed_id }}</a></p>
            @endif      
          </td>
          <td>
            <p>{{ $reference->year }}</p>
          </td>
          <td>
            <p>{{ $reference->reference_name }}</p>
          </td>
          <td>
            <p>{{ $reference->reference_title }}</p>
          </td>
          <td>
            <p>{{ $reference->journal }}</p>
          </td>
          <td>
            <p><a href="{{ url('references/'.$reference->reference_id) }}" ><button class="btn btn-info">Link</button></a></p>
          </td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>


@endsection


@section('scripts')
<script>
  $(function () {
    $('#references').DataTable({
      // dom: 'Brfltip',
      dom: '<lBf<t>ip>',
      buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
      ]
    });
  });
</script>

@endsection