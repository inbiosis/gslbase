<div class="row">
  <div class="col-md-12 px-0">
    @if(count($references))
      <table class="table table-striped">
      <thead>
        <tr>
          <th>Pubmed ID</th>
          <th>Authors</th>  
          <th>Year</th>
          <th>Title</th>
          <th>Journal</th>
          <th>Description</th>
        </tr>
      </thead>
        @foreach ($references as $reference)
        <tr>
          <td>
            <p><a href="https://www.ncbi.nlm.nih.gov/pubmed/?term={{ $reference->pubmed_id }}" target="_blank">{{ $reference->pubmed_id }}</a></p>
          </td>
          <td>
            <p>{{ $reference->reference_name }}</p>
          </td>
          <td>
            <p>{{ $reference->year }}</p>
          </td>
          <td>
            <p>{{ $reference->reference_title }}</p>
          </td>
          <td>
            <p>{{ $reference->journal }}</p>
          </td>
          <td>
            <p><a href="{{ url('references/'.$reference->reference_id) }}" ><button class="btn btn-info">Link</button></a></p>
          </td>
        </tr>
      @endforeach
      </table>
    @else {{-- if no references in search --}}
      <div class="text-center">
        <h3>No references found.</h3>
      </div>
    @endif
  </div>
</div>
