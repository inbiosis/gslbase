@extends('layout.mainlayout')

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <div class="m-b-md jumbotron">
        <h2>Publication Description</h2>
      </div>
      <table class="table profile-table">
        <tr>
          <td class="table-info col-md-2">Authors</td>
          <td class="table-detail col-md-10">{{ $reference->reference_name }}</td>
        </tr>
        <tr>
          <td class="table-info">Title</td>
          <td class="table-detail">{{ $reference->reference_title }}</td>
        </tr>
        <tr>
          <td class="table-info">Journal</td>
          <td class="table-detail">{{ $reference->journal }}</td>
        </tr>
        <tr>
          <td class="table-info">Volume</td>
          <td class="table-detail">{{ $reference->vol }}</td>
        </tr>
        <tr>
          <td class="table-info">DOI</td>
          @if($reference->doi == 'N/A')
          <td class="table-detail">{{ $reference->doi }}</td>
          @else
          <td class="table-detail"><a href="https://doi.org/{{ $reference->doi }}" target="_blank">{{ $reference->doi }}</td>
          @endif
        </tr>
        <tr>
          <td class="table-info">Pubmed ID</td>
          @if($reference->pubmed_id == 'N/A')
          <td class="table-detail">{{ $reference->pubmed_id}}</td>
          @else
          <td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/pubmed/?term={{ $reference->pubmed_id }}" target="_blank">{{ $reference->pubmed_id}}</a></td>
          @endif
        </tr>
        <tr>
          <td class="table-info">PMC ID</td>
          @if($reference->pmc_id == 'N/A')
          <td class="table-detail">{{ $reference->pmc_id}}</td>
          @else
          <td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/pmc/articles/{{ $reference->pmc_id }}" target="_blank">{{ $reference->pmc_id}}</a></td>
          @endif
        </tr>
      </table>

        <ul class="nav nav-tabs">
          <li class="active">
            <a data-toggle="tab" href="#genes">
            Genes{{ count($reference->genes) ? ' ('.count($reference->genes).')' : '' }}
            </a>
          </li>
          <li><a data-toggle="tab" href="#compounds">
            Compounds{{ count($reference->compounds) ? ' ('.count($reference->compounds).')' : '' }}
          </a></li>
        </ul> 
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="genes">
            @include('genes.list', ['genes' => $reference->genes])
          </div>
          <div role="tabpanel" class="tab-pane" id="compounds">
            @include('compounds.list', ['compounds' => $reference->compounds])
          </div>
        </div> 

    </div>
  </div>
</div>
@endsection
