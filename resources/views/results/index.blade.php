@extends('layout.mainlayout')

@section('style')
<style>
    div.DTTT_container {
        margin-left: 1em;
    }
    .dataTables_length {
        float: left !important;
    }
    .dataTables_wrapper .dt-buttons {
        float:right !important;
    }
    .dataTables_filter {
        float: right;
        margin-right: 1em;
    }
</style>
@endsection

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      @if(Request::input('result_type_id')==1)
        <h3 class="jumbotron">Papaya SCC Homologs</h3>
      @elseif(Request::input('result_type_id')==2)
        <h3 class="jumbotron">Cabbage SCC Homologs</h3>
      @elseif(Request::input('result_type_id')==3)   
        <h3 class="jumbotron">Broccoli SCC Homologs</h3>
      @else
        <h3 class="jumbotron">SCC Homologs</h3>
      @endif  
      <table id="results" class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Organism</th>
            <th>Phytozome ID</th>
            <th>Transcript name</th>
            <th>E-Value</th>
            <th>Description</th>
          </tr>
        </thead>
        @foreach ($results as $result)
        <tr>
          <td>
            <p>{{ $result->organism->result_type_name }}</p>
          </td>
          <td>
            <p><a href="https://phytozome.jgi.doe.gov/pz/portal.html#!gene?search=1&detail=1&method=0&searchText=transcriptid:{{ $result->transcript_id }}" target="_blank">{{ $result->transcript_id }}</a></p>
          </td>
          <td>
            <p>{{ $result->transcript_name }}</p>
          </td>
          <td>
            <p>{{ $result->evalue }}</p>
          </td>
          <td>
            <p><a href="{{ url('results/'.$result->id) }}" ><img border="0" alt="W3Schools" src="/assets/phytozome_logo.PNG" width="100"</a></p>
          </td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  $(function () {
    $('#results').DataTable({
      // dom: 'Brfltip',
      dom: '<lBf<t>ip>',
      buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
      ]
    });
  });
</script>

@endsection