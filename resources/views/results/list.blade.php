<div class="row">
  <div class="col-md-12 px-0">
    @if(count($results))
      <table class="table table-striped">
      <thead>
        <tr>
          <th>Organism</th>  
          <th>AGI ID</th>
          <th>Gene Name</th>
          <th>Protein Name</th>
          <th>Identity</th>
          <th>E-Value</th>
          <th>Description</th>
        </tr>
      </thead>
        @foreach ($results as $result)
        <tr>
          <td>
            <p>{{ $result->organism->result_type_name }}</p>
          </td>
          <td>
            <p><a href="http://www.arabidopsis.org/servlets/TairObject?type=locus&name={{ $result->agi_id }}" target="_blank">{{ $result->agi_id }}</a></p>
          </td>
          <td>
            <p>{{ $result->gene_name }}</p>
          </td>
          <td>
            <p>{{ $result->protein_name }}</p>
          </td>
          <td>
            <p>{{ $result->identity }}</p>
          </td>
          <td>
            <p>{{ $result->evalue }}</p>
          </td>
          <td>
            <p><a href="{{ url('results/'.$result->id) }}" ><button class="btn btn-info">Link</button></a></p>  
          </td>
        </tr>
      @endforeach
      </table>
    @else {{-- if no results in search --}}
      <div class="text-center">
        <h3>No homologs found.</h3>
      </div>
    @endif
  </div>
</div>
