@extends('layout.mainlayout')

@section('content')

<div class="album text-muted">
	<div class="container">
		<div class="row">
      <div class="m-b-md jumbotron">
				<h2>{{ $result->organism->result_type_name }} GSL Homolog Description</h2>
			</div>
			<table class="table profile-table">
        <tr>
          <td class="table-info col-md-3">Phytozome ID</td>
          <td class="table-detail col-md-9"><a href="https://phytozome.jgi.doe.gov/pz/portal.html#!gene?search=1&detail=1&method=0&searchText=transcriptid:{{ $result->transcript_id }}" target="_blank">{{ $result->transcript_id }}</a></td>
        </tr>
        <tr>
          <td class="table-info col-md-3">Transcript name</td>
          <td class="table-detail col-md-9">{{ $result->transcript_name }}</td>
        </tr>
				<tr>
					<td class="table-info">Homolog of</td>
					<td class="table-detail"><a href="http://www.arabidopsis.org/servlets/TairObject?type=locus&name={{ $result->agi_id }}" target="_blank">{{ $result->agi_id }}</a></td>
				</tr>
        <tr>
          <td class="table-info">{{ $result->agi_id }} Gene name</td>
          <td class="table-detail">{{ $result->gene_name }}</td>
        </tr>
        <tr>
          <td class="table-info">{{ $result->agi_id }} Protein name</td>
          <td class="table-detail">{{ $result->protein_name }}</td>
        </tr>
				<tr>
          <td class="table-info">BLAST Score</td>
          <td class="table-detail">{{ $result->score }}</td>
        </tr>
        <tr>
          <td class="table-info">E-value</td>
          <td class="table-detail">{{ $result->evalue }}</td>
        </tr>
        <tr>
          <td class="table-info">% Identity</td>
          <td class="table-detail">{{ $result->identity }}</td>
        </tr>
				<tr>
          <td class="table-info">Protein length</td>
          <td class="table-detail">{{ $result->length }}</td>
        </tr>
				</table>			
		</div>
	</div>
</div>

@endsection
