<div class="row">
  <div class="col-md-12 px-0">
    @if(count($genes))
      <table class="table table-striped">
      <thead>
        <tr>
          <th>AGI ID</th>
          <th>Gene Name</th>
          <th>Gene Length</th>
          <th>Protein Name</th>
          <th>Group</th>
          <th>Description</th>
        </tr>
      </thead>
        @foreach ($genes as $gene)
        <tr>
          <td>
            <p><a href="http://www.arabidopsis.org/servlets/TairObject?type=locus&name={{ $gene->agi_id }}" target="_blank">{{ $gene->agi_id }}</a></p>
          </td>
          <td>
            <p>{{ $gene->gene_name }}</p>
          </td>
          <td>
            <p>{{ $gene->length }}</p>
          </td>
          <td>
            <p>{{ $gene->protein_name }}</p>
          </td>
          <td>
            <p>{{ $gene->group }}</p>
          </td>
          <td>
            <p><a href="{{ url('genes/'.$gene->agi_id) }}" ><button class="btn btn-info">Link</button></a></p>  
          </td>
        </tr>
      @endforeach
      </table>
    @else {{-- if no genes in search --}}
      <div class="text-center">
        <h3>No genes found.</h3>
      </div>
    @endif
  </div>
</div>
