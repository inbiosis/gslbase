@extends('layout.mainlayout')

@section('style')
<style>
    div.DTTT_container {
        margin-left: 1em;
    }
    .dataTables_length {
        float: left !important;
    }
    .dataTables_wrapper .dt-buttons {
        float:right !important;
    }
    .dataTables_filter {
        float: right;
        margin-right: 1em;
    }
</style>
@endsection

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <h3 class="jumbotron">Known SCC Genes</h3>  
      <table id="genes" class="table table-striped table-hover">
        <thead>
          <tr>
            <th>AGI ID</th>
            <th>UniProt ID</th>
            <th>EnsemblPlants</th>
            <th>Gene Name</th>
            <th>Protein Name</th>
            <th>Function</th>
            <th>Description</th>
          </tr>
        </thead>
        @foreach ($genes as $gene)
        <tr>
          <td>
            <p><a href="http://www.arabidopsis.org/servlets/TairObject?type=locus&name={{ $gene->agi_id }}" target="_blank">{{ $gene->agi_id }}</a></p>
          </td>
          <td>
            <p><a href="http://www.uniprot.org/uniprot/{{ $gene->uniprot }}" target="_blank">{{ $gene->uniprot }}</a></p>
          </td>
          <td>
            <p><a href="http://plants.ensembl.org/Arabidopsis_thaliana/Gene/Summary?g={{ $gene->agi_id}}"><span class="glyphicon glyphicon-link"></span> Link</a></p>
          </td>
          <td>
            <p>{{ $gene->gene_name }}</p>
          </td>
          <td>
            <p>{{ $gene->protein_name }}</p>
          </td>
          <td>
            <p>{{ $gene->function }}</p>
          </td>
          <td>
            <p><a href="{{ url('genes/'.$gene->agi_id) }}" ><button class="btn btn-info">Link</button></a></p>  
          </td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>
@endsection


@section('scripts')
<script>
  $(function () {
    $('#genes').DataTable({
      dom: '<lBf<t>ip>',
      buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
      ]
    });
  });
</script>

@endsection