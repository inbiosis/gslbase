@extends('layout.mainlayout')

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <div class="m-b-md jumbotron">
        <h2>Putative SCC Gene Description</h2>
      </div>
        <table class="table profile-table">
          <tr>
            <td class="table-info col-md-2">Computed by</td>
            <td class="table-detail col-md-10">{{ $putative->type->interaction_type_name }}</td>
          </tr>
          <tr>
            <td class="table-info col-md-2">Gene name</td>
            <td class="table-detail col-md-10">{{ $putative->gene_name }}</td>
          </tr>
          <tr>
            <td class="table-info">AGI ID</td>
            <td class="table-detail"><a href="http://www.arabidopsis.org/servlets/TairObject?type=locus&name={{ $putative->agi_id }}" target="_blank">{{ $putative->agi_id }}</a></td>
          </tr>
          <tr>
            <td class="table-info">Gene length</td>
            <td class="table-detail">{{ $putative->length }}</td>
          </tr>
          <tr>
            <td class="table-info">Uniprot ID</td>
            <td class="table-detail"><a href="http://www.uniprot.org/uniprot/{{ $putative->uniprot }}" target="_blank">{{ $putative->uniprot }}</a></td>
          </tr>
          <tr>
            <td class="table-info">Protein Name</td>
            <td class="table-detail">{{ $putative->protein_name }}</td>
          </tr>
          <tr>
            <td class="table-info">Synonym</td>
            <td class="table-detail">{{ $putative->synonym }}</td>
          </tr>
          <tr>
            <td class="table-info">EC number</td>
            <td class="table-detail"><a href="https://enzyme.expasy.org/EC/{{ $putative->ec_number }}" target="_blank">{{ $putative->ec_number }}</a></td>
          </tr>
          <tr>
            <td class="table-info">Entrez Gene</td>
            <td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/gene/{{ $putative->entrez_gene }}" target="_blank">{{ $putative->entrez_gene}}</a></td>
          </tr>
          <tr>
            <td class="table-info">Refseq mrna</td>
            <td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/nuccore/{{ $putative->refseq_mrna }}" target="=_blank">{{ $putative->refseq_mrna }}</a></td>
          </tr>
          <tr>
            <td class="table-info">Refseq protein</td>
            <td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/protein/{{ $putative->refseq_protein }}" target="_blank">{{ $putative->refseq_protein }}</a></td>
          </tr>
        </table>


      <ul class="nav nav-tabs">
        <li class="active">
          <a data-toggle="tab" href="#geneontologies">
            Gene Ontologies{{ count($putative->geneontologies) ? ' ('.count($putative->geneontologies).')' : '' }}
          </a>
        </li>
      </ul> 

      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="geneontologies">
          @include('geneontologies.list', ['geneontologies' => $putative->geneontologies])
        </div>
      </div>

    </div>
  </div>
</div>
@endsection
