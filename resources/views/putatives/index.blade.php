@extends('layout.mainlayout')

@section('style')
<style>
    div.DTTT_container {
        margin-left: 1em;
    }
    .dataTables_length {
        float: left !important;
    }
    .dataTables_wrapper .dt-buttons {
        float:right !important;
    }
    .dataTables_filter {
        float: right;
        margin-right: 1em;
    }
</style>
@endsection

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <h3 class="jumbotron">Putative SCC Genes</h3>  
      <table id="putatives" class="table table-striped table-hover">
        <thead>
          <tr>
            <th>AGI ID</th>
            <th>Uniprot ID</th>
            <th>Gene Name</th>
            <th>Protein Name</th>
            <th>SCC Genes Type</th>
            <th>Description</th>
          </tr>
        </thead>
        @foreach ($putatives as $putative)
        <tr>
          <td>
            <p><a href="http://www.arabidopsis.org/servlets/TairObject?type=locus&name={{ $putative->agi_id }}" target="_blank">{{ $putative->agi_id }}</a></p>
          </td>
          <td>
            <p><a href="http://www.uniprot.org/uniprot/{{ $putative->uniprot }}" target="_blank">{{ $putative->uniprot }}</a></p>
          </td>
          <td>
            <p>{{ $putative->gene_name }}</p>
          </td>
          <td>
            <p>{{ $putative->protein_name }}</p>
          </td>
          <td>
            <p>{{ $putative->gsl_type}}</p>
          </td>
          <td>
            <p><a href="{{ url('putatives/'.$putative->agi_id) }}" ><button class="btn btn-info">Link</button></a></p>  
          </td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>

@endsection


@section('scripts')
<script>
  $(function () {
    $('#putatives').DataTable({
      // dom: 'Brfltip',
      dom: '<lBf<t>ip>',
      buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
      ]
    });
  });
</script>

@endsection