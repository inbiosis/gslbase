@extends('layout.mainlayout')

@section('content')

<div class="album text-muted">
	<div class="container">
		<div class="row">
			<div class="m-b-md jumbotron">
				<h2>SCC Gene Description</h2>
			</div>
			<table class="table profile-table">
				<tr>
					<td class="table-info col-md-2">Gene name</td>
					<td class="table-detail col-md-10">{{ $gene->gene_name }}</td>
				</tr>
				<tr>
					<td class="table-info col-md-2">AGI ID</td>
					<td class="table-detail"><a href="http://www.arabidopsis.org/servlets/TairObject?type=locus&name={{ $gene->agi_id }}" target="_blank">{{ $gene->agi_id }}</a></td>
				</tr>
				<tr>
					<td class="table-info">Gene length</td>
					<td class="table-detail">{{ $gene->length }}</td>
				</tr>
				<tr>
					<td class="table-info">Uniprot ID</td>
					<td class="table-detail"><a href="http://www.uniprot.org/uniprot/{{ $gene->uniprot }}" target="_blank">{{ $gene->uniprot }}</a></td>
				</tr>
				<tr>
					<td class="table-info">Protein Name</td>
					<td class="table-detail">{{ $gene->protein_name }}</td>
				</tr>
				<tr>
					<td class="table-info">Synonym</td>
					<td class="table-detail">{{ $gene->synonym }}</td>
				</tr>
				<tr>
					<td class="table-info">EC number</td>
					@if($gene->ec_number == 'N/A')
					<td class="table-detail">{{ $gene->ec_number }}</td>
					@else
					<td class="table-detail"><a href="https://enzyme.expasy.org/EC/{{ $gene->ec_number }}" target="_blank">{{ $gene->ec_number }}</a></td>
					@endif      
				</tr>
				<tr>
					<td class="table-info">Entrez Gene</td>
					@if( $gene->entrez_gene == 'N/A')
					<td class="table-detail">{{ $gene->entrez_gene }}</td>
					@else
					<td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/gene/{{ $gene->entrez_gene }}" target="_blank">{{ $gene->entrez_gene}}</a></td>
					@endif
				</tr>
				<tr>
					<td class="table-info">Refseq mrna</td>
					@if( $gene->refseq_mrna == 'N/A')
					<td class="table-detail">{{ $gene->refseq_mrna }}</td>
					@else
					<td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/nuccore/{{ $gene->refseq_mrna }}" target="=_blank">{{ $gene->refseq_mrna }}</a></td>
					@endif
				</tr>
				<tr>
					<td class="table-info">Refseq protein</td>
					@if($gene->refseq_protein == 'N/A')
					<td class="table-detail">{{ $gene->refseq_protein }}</td>
					@else
					<td class="table-detail"><a href="https://www.ncbi.nlm.nih.gov/protein/{{ $gene->refseq_protein }}" target="_blank">{{ $gene->refseq_protein }}</a></td>
					@endif
				</tr>
				<tr>
					<td class="table-info">Function</td>
					<td class="table-detail">{{ $gene->function }}</td>
				</tr>
				<tr>
					<td class="table-info">Group</td>
					<td class="table-detail">{{ $gene->group }}</td>
				</tr>
				<tr>
					<td class="table-info">Reference</td>
					<td class="table-detail">{{ $gene->reference }}</td>
				</tr>
			</table>

			<ul class="nav nav-tabs">
				<li class="active">
					<a data-toggle="tab" href="#homologs">
						Homologs{{ count($gene->results) ? ' ('.count($gene->results).')' : '' }}
					</a>
				</li>
				<li><a data-toggle="tab" href="#references">
					References{{ count($gene->references) ? ' ('.count($gene->references).')' : '' }}
				</a></li>
			</ul> 

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="homologs">
					@include('results.list', ['results' => $gene->results])
				</div>
				<div role="tabpanel" class="tab-pane" id="references">
					@include('references.list', ['references' => $gene->references])
				</div>
			</div> 

		</div>
	</div>
</div>

@endsection
