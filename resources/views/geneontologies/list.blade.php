<div class="row">
  <div class="col-md-12 px-0">
    @if(count($geneontologies))
      <table class="table table-striped">
      <thead>
        <tr>
          <th>GO ID</th>
          <th>Ontology</th>
          <th>GO Term</th>
          <th>Description</th>
        </tr>
      </thead>
        @foreach ($geneontologies as $geneontology)
        <tr>
          <td>
            <p><a href="http://amigo.geneontology.org/amigo/term/{{ $geneontology->go_id }}" target="_blank">{{ $geneontology->go_id }}</p>
          </td>
          <td>
            <p>{{ $geneontology->ontology }}</p>
          </td>
          <td>
            <p>{{ $geneontology->go_term }}</p>
          </td>
          <td>
            <p>{{ $geneontology->go_description }}</p>
          </td>
        </tr>
      @endforeach
      </table>
    @else {{-- if no genes in search --}}
      <div class="text-center">
        <h3>No genes found.</h3>
      </div>
    @endif
  </div>
</div>
