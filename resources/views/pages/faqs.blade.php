@extends('layout.sidebar')

@section('content')

<div class="album text-muted">
  <div class="container" style="margin-bottom: 60px;">

    <div class="row">
      <div class="col-sm-12 px-0">
        <h2>FAQs</h2>
        <ol>
         <b><li>What is SuCComBase?</li></b>
         <p class="text-justify">SuCComBase is a database that compose all the information of known, putative and potential sulfur-containing compounds (SCCs) biosynthetic genes from different public databases and literature. SuCComBase also provides additional information such as SCC homologs, SCC compounds and publications that related with SCC genes.</p>

         <b><li>What is potential SCC gene?</li></b>
         <p class="text-justify">Potential gene is predicted as gene that co-expressed with the known SCC genes. Gene co-expression study employed to identified genes that are potentially involved in sulfur-containing compounds (SCCs). Genes within modules of a co-expression network may be involved in similar biological processes and carry same biological functions.</p>

         <b><li>How does SuCComBase identify putative and potential gene?</li></b>
         <p class="text-justify">Putative SCC gene is a predicted gene with computational evidence retrieved from two public database, Kyoto Encyclopedia of Genes and Genomes (KEGG) and AraCyc. Whilst, potential gene is a gene that co-expressed with known SCC gene identified from co-expression network study. All the data sources of co-expression network were collected from AraNet, GeneMania and ATTED databases.</p>

         <b><li>What gene identifiers does SuCComBase recognize?</li></b>
         <p class="text-justify">SuCComBase recognizes gene name, AGI ID, Uniprot ID, Entrez, and Refseq.</p>

         <b><li>How can I download the datasets in SuCComBase?</li></b>
         <p class="text-justify">User can download the datasets by click the “CSV/Excel” button provided at the top right in each of SuCComBase table.
         </p>
       </ol>
     </div>
   </div>
 </div> 
</div>

@endsection


