@extends('layout.sidebar')

@section('content')

<div class="album text-muted">
  <div class="container" style="margin-bottom: 60px;">

    <div class="row">
      <div class="col-sm-12 px-0">
        <h2>Glossary</h2>
        
        <ul>
        <li><b>AGI ID</b>: Locus identifier for Arabidopsis Genome Initiative (TAIR, TIGR and MIPS databases).</li>
        <li><b><i>Arabidopsis thaliana</i></b>: Small flowering plant that is widely used as a model organism in plant biology.</li>
		<li><b><i>Brassicaceae</i></b>: Formerly known as Cruciferae, the mustard family of flowering plants that includes many plants of economic importance that have been extensively altered and domesticated by humans, especially those of the genus Brassica, which includes cabbage, broccoli, Brussels sprouts, kale, kohlrabi, napa cabbage, turnip, and rutabaga. Other important agricultural crops in the family include horseradish, radish, and white mustard. </li>
		<li><b>Brassicales</b>: Order of flowering plants that includes cabbages and capers, as well as mignonette, mustard, and nasturtiums. Brassicales includes 17 families, 398 genera, and 4,450 species. </li>
		<li><b>Camalexin</b>: One of secondary metabolites that contains sulfur and functions as phytoalexin to deter pests. Exist specifically in <i>A. thaliana</i>.</li>
		<li><b>Co-expression</b>: The simultaneous expression of two or more genes under specific condition or treatment.</li>
		<li><b>Compound</b>: Also known as metabolite, is the intermediate end product of metabolism.</li>
		<li><b>Databases</b>: A collection of information that is organized and structured so that it can be easily accessed, managed and updated.</li>
		<li><b>EC number</b>: A numerical classification scheme for enzymes, based on the chemical reactions they catalyze. EC number is associated with a recommended name for the respective enzyme.</li>
		<li><b>Entrez ID</b>: Gene identifier from NCBI database.</li>
		<li><b>Gene</b>: A gene is the basic physical and functional unit of heredity. Genes, which are made up of DNA, act as instructions to make molecules called proteins.</li>
		<li><b>Genome</b>: Complete set of genetic material of organism, including genes (the coding regions) and the noncoding DNA as well as the genetic material of the mitochondria and chloroplasts.</b>
		<li><b>Glucosinolate</b>: One of the secondary metabolites that contains sulfur and nitrogen which is derived from glucose and an amino acid. Contribute to plant defense against pests and pathogens.</li>
		<li><b>Group</b>: Type of sulfur-containing compounds (SCCs) biosynthesis (i.e. camalexin or glucosinolate biosynthesis).</li>
		<li><b>Homologs</b>: Two or more homologous/similar genes with DNA sequence derives from a common origin and may or may not have the same function.</li>
		KNApSAcK ID: Identifier of plant metabolites database, KNApSAcK. 
		<li><b>Phytoalexin</b>: One of secondary metabolites that contains sulfur and has antimicrobial property.</li>
		<li><b>Pubmed ID</b>: Identifier of a free search engine, PUBMED, accessing full-text journal literature of biomedical and life sciences. </li>
		<li><b>Putative genes</b>: Putative genes in SuCComBase are defined as computationally predicted A. thaliana SCC genes based on their protein sequences from KEGG and AraCyc database.</li>
		<li><b>Potential genes</b>: Potential SCC genes in A. thaliana that were calculated using co-expression data from AraNet, GeneMANIA and ATTED.</li>
		<li><b>Pubmed ID</b>: Identifier of a free search engine, PUBMED, accessing full-text journal literature of biomedical and life sciences. </li>
		<li><b>Refseq ID</b>: Identifier from The Reference Sequence (Refseq) database.</li>
		<li><b>Secondary metabolite</b>: Secondary metabolites (SMs) are generally defined as small organic molecules produced by an organism that are not essential for their growth, development and reproduction.</li>
		<li><b>UniProt ID</b>: Protein identifier from UniProt database.</li>
      </div>
    </div>
  </div> 
</div>

@endsection


