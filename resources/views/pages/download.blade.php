@extends('layout.mainlayout')

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <h3>Download SuCComBase</h3>
            <div class="datasets col-md-8 px-0" style="margin-top: 10px">
        <table class="table">
          <thead class="stats-header">
            <tr>
              <th class="col-md-1">Version</th>
              <th class="col-md-1">Date</th>
              <th class="col-md-4">Changelog</th>
              <th class="col-md-1">Excel(Zipped)</th>
              <th class="col-md-1">Sql</th>
            </tr>
          </thead>
          <tbody>
             <tr>
              <td>v1.2</td>
              <td>4th January 2019</td>
              <td>Database changes:<br>
              - added 32 new compounds data<br>
              - added SCC function in each compound description page<br>

              </td>
              <td><a href="{{ url('db/succombase-v1.2-excel.zip')}}"><button class="btn btn-primary">Link</button></a></td>
              <td><a href="{{ url('db/succombase-v1.2.sql')}}"><button class="btn btn-warning">Link</button></a></td>
            </tr>
            <tr>
              <td>v1.1</td>
              <td>16th October 2018</td>
              <td>Added links to:<br>
               - www.arabidopsis.org for each AGI ID<br>
               - www.uniprot.org for each UniProt ID<br>
               - www.ebi.ac.uk/ChEBI for each ChEBI ID<br>
               - kanaya.naist.jp/KNApSAcK/ for each KNApSAcK ID<br>
               Layout changes:<br>
               - changed URL/Link format for easier differentiation<br>
               - removed clickable rows in tables and replaced with Link button<br>
               - added download page to download SuCComBase Excel/Sql files<br>
               Database changes:<br>
               - added Gene Ontologies table 
              </td>
              <td><a href="{{ url('db/succombase-v1.1-excel.zip')}}"><button class="btn btn-primary">Link</button></a></td>
              <td><a href="{{ url('db/succombase-v1.1.sql')}}"><button class="btn btn-warning">Link</button></a></td>
            </tr>
            <tr>
              <td>v1.0</td>
              <td>4th July 2018</td>
              <td>Initial verison of SuCComBase</td>
              <td><a href="{{ url('db/succombase-v1.0-excel.zip')}}"><button class="btn btn-primary">Link</button></a></td>
              <td><a href="{{ url('db/succombase-v1.0.sql')}}"><button class="btn btn-warning">Link</button></a></td>
            </tr>
            
           
          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>

@endsection
