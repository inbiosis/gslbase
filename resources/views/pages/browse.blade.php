@extends('layout.mainlayout')

@section('content')

    <div class="album text-muted">
      <div class="container">
        <div class="row">
          <h3>Browse SuCComBase</h3>
          {{-- Dataset descriptions --}}
          <div class="box">
            <div class="box-body no-padding">
              <table class="table">
                <tbody>
                  <tr>
                    <td class="col-md-4"><a href="{{ url('/genes') }}">Known <i>Arabidopsis thaliana</i> SCC genes</a></td>
                    <td class="col-md-8">Genes known to involve in SCC biosynthesis through experiments</td>
                  </tr>
                  <tr>
                    <td class="col-md-4"><a href="{{ url('/putatives') }}">Putative <i>Arabidopsis thaliana</i> SCC genes</a></td>
                    <td class="col-md-8">Computationally predicted SCC genes</td>
                  </tr>
                  <tr>
                    <td class="p-l-sm col-md-4"><a href="{{ url('/putatives?interaction_comp_type=1') }}">KEGG Putative <i>Arabidopsis thaliana</i> SCC genes</a></td>
                    <td class="col-md-8">Computationally predicted SCC genes from KEGG database</td>
                  </tr>
                  <tr>
                    <td class="p-l-sm col-md-4"><a href="{{ url('/putatives?interaction_comp_type=2') }}">AraCyc Putative <i>Arabidopsis thaliana</i> SCC genes</a></td>
                    <td class="col-md-8">Computationally predicted SCC genes from AraCyc database</td>
                  </tr>
                  <tr>
                    <td class="col-md-4"><a href="{{ url('/coexpressions') }}">Potential SCC Genes</a></td>
                    <td class="col-md-8">Genes that co-expressed with the known SCC genes</td>
                  </tr>
                  <tr>
                    <td class="col-md-4"><a href="{{ url('/results') }}">SCC Homologs</a></td>
                    <td class="col-md-8">SCC genes identified in three <i>Brassicales</i> plants</td>
                  </tr>
                  <tr>
                    <td class="p-l-sm col-md-4"><a href="{{ url('/results?result_type_id=1') }}"><i>Carica papaya</i> SCC Homologs</a></td>
                    <td class="col-md-8">SCC homologs identified in the <i>Carica papaya</i> genome</td>
                  </tr>
                  <tr>
                    <td class="p-l-sm col-md-4"><a href="{{ url('/results?result_type_id=2') }}"><i>Brassica rapa</i> SCC Homologs</a></td>
                    <td class="col-md-8">SCC homologs identified in the <i>Brassica rapa</i> genome</td>
                  </tr>
                  <tr>
                    <td class="p-l-sm col-md-4"><a href="{{ url('/results?result_type_id=3') }}"><i>Brassica oleracea</i> SCC Homologs</a></td>
                    <td class="col-md-8">SCC homologs identified in the <i>Brassica oleracea</i> genome</td>
                  </tr>
                  <tr>
                    <td class="col-md-4"><a href="{{ url('/compounds') }}">Compounds</a></td>
                    <td class="col-md-8">SCC compounds identified from literature and KNApSAcK database</td>
                  </tr>
                  <tr>
                    <td class="p-l-sm col-md-4"><a href="{{ url('/compounds?result_type_id=4') }}"><i>Arabidopsis thaliana</i> Compounds</a></td>
                    <td class="col-md-8">SCC compounds that specifically produced by <i>Arabidopsis thaliana</i></td>
                  </tr>
                  <tr>
                    <td class="p-l-sm col-md-4"><a href="{{ url('/compounds?result_type_id=1') }}"><i>Carica papaya</i> Compounds</a></td>
                    <td class="col-md-8">SCC compounds that specifically produced by <i>Carica papaya</i></td>
                  </tr>
                  <tr>
                    <td class="p-l-sm col-md-4"><a href="{{ url('/compounds?result_type_id=2') }}"><i>Brassica rapa</i> Compounds</a></td>
                    <td class="col-md-8">SCC compounds that specifically produced by <i>Brassica rapa</i></td>
                  </tr>
                  <tr>
                    <td class="p-l-sm col-md-4"><a href="{{ url('/compounds?result_type_id=3') }}"><i>Brassica oleracea</i> Compounds</a></td>
                    <td class="col-md-8">SCC compounds that specifically produced by <i>Brassica oleracea</i></td>
                  </tr>
                  <tr>
                    <td class="col-md-4"><a href="{{ url('/references') }}">Publications</a></td>
                    <td class="col-md-8">List of publications related to SCC biosynthesis in <i>Brassicales</i></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
