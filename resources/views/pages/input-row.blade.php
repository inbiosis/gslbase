<div class="form-group multi-field">
  <div class="col-sm-1">
    <select class="form-control" id="sel1" name="operator[]">
      <option>AND</option>
      <option>OR</option>
      <option>NOT</option>
    </select>
  </div>
  <div class="col-sm-2">
    <select class="form-control" id="sel2" name="field[]">
      <option>All Fields</option>
      <option>Protein Description</option>
      <option>Ontology</option>
      <option>GO Term</option>
      <option>GO Definition</option>
      <option>Domain Description</option>
      <option>Pathway Term</option>
      <option>Pathway Source</option>
      <option>Disease Name</option>
      <option>Tissue</option>
      <option>Cell Type</option>
    </select>
  </div>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="inputEmail3" name="search_terms[]" value="{{ Request::input('search_terms')[$index] ? Request::input('search_terms')[$index] : '' }}">
  </div>
  <button type="button" class="btn btn-default remove-field">
    <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
  </button>
</div>
