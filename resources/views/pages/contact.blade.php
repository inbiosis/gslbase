@extends('layout.mainlayout')

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <h3>Contact Us</h3>
      <p>Bioinformatics & Computational Systems Biology Research Group (BCSB),<br>
        Center for Bioinformatics Research (CBR),<br>
        Institute of Systems Biology (INBIOSIS),<br>
        Universiti Kebangsaan Malaysia,<br>
        43600 UKM Bangi,<br> 
        Malaysia.<br>
        <br>
        Tel: +6 03 8921 4547/5993<br>
      Email: succombase(at)gmail.com</p>       
    </div>
  </div>
</div>

@endsection
