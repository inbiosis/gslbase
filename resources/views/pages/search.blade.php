@extends('layout.mainlayout')

@section('content')
<div class="album text-muted">
  <div class="container" style="margin-bottom: 60px;">
    <h3 class="m-t-md">Search SuCComBase</h3>

    @include('pages.search-simple-form')

  </div>
</div>

@endsection

@section('scripts')
<script>
$('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});
</script>
@endsection
