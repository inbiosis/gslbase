@extends('layout.mainlayout')

@section('content')

<div class="album text-muted">
  <div class="container" style="margin-bottom: 60px;">

    <div class="row">
      <div class="col-sm-12 px-0">
        <h2>About SuCComBase</h2>
        <p class="text-justify">This database houses public information regarding components that involved in the production of sulfur-containing compounds (SCC)s. Glucosinolates (GSL)s and phytoalexins are examples of SCCs in <i>Brassicales</i>. GSLs are sulphur and nitrogen-containing compounds known for their role in plant defence against herbivores and pathogens. They are found widely in the <i>Brassicaceae</i> family and in the model plant, <i>Arabidopsis thaliana</i>. These <i>Brassicaceae</i> are cruciferous vegetables that include broccoli, cabbage, cauliflower, kale, mustard and cress. Amongst secondary metabolites, GSLs are mostly studied since starting of the year 2000 until now. Interest in plant GSLs has grown rapidly in recent years not only because of their importance in plant defence, they are also investigated because of their role as cancer preventive agents.</p>  

        <p class="text-justify">SuCComBase is developed to provide a one-stop-centre to researchers that are interested to investigate all of the components (genes and compounds) in four selected plants (<i>Arabidopsis thaliana, Brassica rapa, Brassica oleracea </i> and <i> Carica papaya</i>).</p>  
      </div>
    </div>

    <div class="row">
      <div class="col-sm-6 pl-0">  
        <h3><i>Arabidopsis thaliana</i></h3>
        <img src="/assets/arabidopsis_background.jpg" width="250" class="pull-right gap-left img-thumbnail">    
        <p class="text-justify"><i>Arabidopsis thaliana</i> is also known as thale cress, mouse-ear cress that is a model organism in plant biology. On 2001, 34 glucosinolates were identified from the leaves and seeds of 39 different Arabidopsis ecotypes where most of them are chain-elongated products originated from methionine (Kliebenstein et al. 2001). All of the compounds can be assessed from SuCComBase together with latest studies conducted in exploring new GSL compounds.</p>
      </div>
      <div class="col-sm-6 pr-0">
        <h3><i>Brassica rapa</i></h3> 
        <img src="/assets/B_rapa.png" width="250" class="pull-right gap-left img-thumbnail"> 
        <p class="text-justify"><i>Brassica rapa</i>, field mustard is a herbaceous species where various varieties have been developed that include turnips, bok choi and Chinese cabbages. Aliphatic glucosinolates are the most abundant in this plant where indolic and aromatic glucosinolates were low (Padilla et al. 2007). </p>   
      </div>
    </div>

    <div class="row">
      <div class="col-sm-6 pl-0">
        <h3><i>Brassica oleracea</i></h3>
        <img src="/assets/B_capitata.jpg" width="250" class="pull-right gap-left img-thumbnail">
        <p class="text-justify"><i>Brassica oleracea</i>, wild cabbage, is another species from the Brassicaceae that includes many common foods as cultivars such as broccoli, cabbage, cauliflower, collards, Chinese broccoli, Brussels sprouts, kale, curly kale, and kohlrabi, among others. </p> 
      </div>
      <div class="col-sm-6 pr-0">
        <h3><i>Carica papaya</i></h3>
        <img src="/assets/papaya.jpg" width="250" class="pull-right gap-left img-thumbnail" >
        <p class="text-justify"><i>Carica papaya</i>, belongs to the Caricaceae family that also included in Brassicales order together with the Brassicaceae family. Glucotropaeolin (aromatic GSL) was reported in <i>Carica papaya</i> where the highest concentration was found in the youngest leaves followed with other plant tissues (Bennet et al. 1997).</p>
      </div>
    </div>

  </div> 
</div>

@endsection


