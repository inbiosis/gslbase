<form method="get" action="{{ url('search') }}" class="form-horizontal">
  <input type="hidden" name="search_mode" value="simple">
  <div class="form-group">
    <div class="col-sm-12">
      <input type="text" class="form-control" id="inputEmail3" name="search_term" value="{{ Request::input('search_term') }}">
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-12">
      <button type="submit" class="btn btn-default">Search</button>
    </div>
  </div>
</form>

@if(Request::input('search_mode') == 'simple')
<!-- <div class="row">
  <div class="col-md-12"> -->
    <table class="table">
        <thead>
           <tr>
            <th>Dataset</th>
            <th>Number of Entries</th>
           </tr>
        </thead>
        @foreach($results as $result)
     <tbody>
     <td>
      {{ $result['name'] }}
     </td>
     <td>
        <a href="{{ url(strtolower($result['url']).'?search_term='.Request::input('search_term')) }}">
          Found {{ $result['count'] }} {{ $result['count'] == 1 ? 'entry' : 'entries' }}.
        </a>
     </td>
     </tbody>
        @endforeach
    </table>
  </div>
</div>
@endif