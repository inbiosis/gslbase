@extends('layout.sidebar')

@section('content')

<div class="album text-muted">
  <div class="container" style="margin-bottom: 60px;">

    <div class="row">
      <div class="col-sm-12 px-0">
        <h2>SuCComBase Database</h2>
<ol>
        <h3><li>SuCComBase Schema</li></h3>
        <p class="text-justify">SuCComBase currently consists of 10 tables. Figure 1 shows all the 10 tables with the connections between table to table.</p> 

        <img src="/assets/schema.png" width="1200" class="pull-right gap-left img-thumbnail"> 
        <div align="center" >
			Figure 1. SuCComBase schema
		</div>
		

		<h3><li>Table Information</li></h3>
		<ol>

		<li><b>genes</b>: This is the main table of SuCComBase. This table has basic information including Entrez Gene ID, UniProt ID, gene symbol and gene function. It contains a total of 147 SCC-related genes in <i>A. thaliana</i> involving genes that encode proteins that involve in the glucosinolate and camalexin biosynthetic pathways based on experiments reported in various publications and pathway databases (KEGG and AraCyc).</li>
		<li><b>genes_references</b>: This is the pivot table between genes and references.</li>
		<li><b>references</b>: This table contains 196 published literature that were used to identify SCCs as well as known SCC-related genes in <i>A. thaliana</i> that is the main data in SuCComBase.</li>
		<li><b>results</b>: This table contains a total of 4026 orthologous SCC-related genes in papaya (<i>C. papaya</i>), cabbage (<i>B. rapa</i>) and broccoli (<i>B. oleracea</i>). These orthologues were identified from sequence similarity search using known SCC-related genes in <i>A. thaliana</i> against the three Brassicales plants. The cutoff values are sequence identity over 40% and e-value lesser than 1e-50.</li>
		<li><b>organisms</b>: This table lists all information of different Brassicales in SuCComBase: papaya (<i>C. papaya</i>), cabbage (<i>B. rapa</i>) and broccoli (<i>B. oleracea</i>).</li>
		<li><b>coexpressions</b>: This table contains 778 <i>A. thaliana</i> genes that may involve in SCC biosynthetic pathway based on bioinformatic analysis using coexpression data retrieved from AraNet, GeneMANIA and ATTED. These genes are known as potential GSL genes that may also involve in GSL biosynthesis.</li>
		<li><b>putatives</b>: This table contains 92 computationally predicted glucosinolate and camalexin genes from the pathway databases (KEGG and AraCyc).</li>
		<li><b>putatives_type</b>: This table connects putatives and types. </li>
		<li><b>compounds</b>: contains a total of 84 SCCs specifically produced by <i>A. thaliana</i> (42 SCCs), papaya (5 SCCs), cabbage (16 SCCs) and broccoli (21 SCCs). All of the information were generated from publications and several of them can be also identified from the KNApSAcK database.</li>
		<li><b>compounds_references</b>: This table links compounds with references that corroborate the production SCCs from publications.</li>

        </ol>

        <h3><li>Data Types Structure organization</li></h3>
        <p class="text-justify">In SuCComBase, there are three types of data that are the SCC genes, SCCs and references. Figure 2 shows the organization of the data types where SCC genes in <i>A. thaliana</i> are the main data in SuCComBase.</p> 

         <img src="/assets/succombase_data.png" width="1200" class="pull-right gap-left img-thumbnail"> 
        <div align="center" >
			Figure 2. SuCComBase data types structure organization
		</div>
	</ol>

      </div>
    </div>

   

  </div> 
</div>

@endsection


