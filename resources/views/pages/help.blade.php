@extends('layout.sidebar')

@section('content')

<div class="album text-muted">
	<div class="container" style="margin-bottom: 60px;">

		<div class="row">
			<div class="col-sm-12 px-0">
				<h2>SuCComBase Manual</h2>
				<ol>
					<h3><li>Site Organization</li></h3>
					<br>
					<img src="/assets/succombase_structure.jpg" width="1200" class="pull-right gap-left img-thumbnail"> 
					<div align="center" >
						Figure 1. The structure of SuCComBase.
					</div><br>

					<p class="text-justify">Figure 1 shows the integration of the tables entry in SuCComBase. The main table of SuCComBase is SCC genes/known Arabidopsis SCC genes table, which 147 genes are recorded. The blue-coloured boxes represent the tables that described SCC genes entity (e.g: Compounds, Publications, SCC homologs, etc).</p>

					<ul>
						<li>The <b>Compounds</b> table contains all the SCC compounds identified from literature and KNApSAcK database. SCC compounds information specifically produced by <i>Arabidopsis thaliana</i>, <i>Carica papaya</i>, <i>Brassica rapa</i>, and <i>Brassica oleracea</i> were collected. </li>

						<li><b>SCC genes</b> table listed all the known Arabidopsis genes to involve in SCC biosynthesis through experiments.</li>

						<li><b>Putative genes</b> table consists of all putative Arabidopsis SCC genes which computationally predicted from metabolic pathway databases, Kyoto Encyclopedia of Genes and Genomes (KEGG) and AraCyc.</li>

						<li><b>Potential genes</b> table listed all the genes that co-expressed with the known SCC genes. The co-expression network data were obtained from three public databases, AraNet, GeneMania and ATTED. </li>

						<li><b>SCC homologs</b> table contains SCC genes identified in other three Brassicales plants. All SCC homologs were collected from the sequence homology searching activity against three plant genomes, <i>Carica papaya</i>, <i>Brassica rapa</i>, and <i>Brassica oleracea</i> using Phytozome database Blast tool.</li> 

						<li><b>Publications</b> table comprised all the publications related to SCC biosynthetic genes in Brassicales.</li>
					</ul>

					<h3><li>Search Box</li></h3>
					
					<p class="text-justify">SuCComBase can be used to search for genes, compounds, publications and homologs ID to match a keyword. User can also search the specific ID or keyword using the search box.</p>
					<br>

					<ul>
						<li>AGI ID</li>
						<li>Uniprot ID</li>
						<li>Refseq ID</li>
						<li>Gene name</li>
						<li>Compound name or common name</li>
						<li>Compound formula</li>
						<li>Knapsack ID</li>
						<li>Keyword, e.g. glucosinolate</li>

						<br>
						<p class="text-justify">Figure 2 shows the page of SuCComBase with the ‘glucosinolate’ keyword as input in the search box. All entries in the datasets (genes, putative genes, potential genes, compounds, etc) will be shown once “glucosinolate” keyword is searched.</p>
						<br>


						<img src="/assets/site_figure2.jpg" width="1200" class="pull-right gap-left img-thumbnail"> 
						<div align="center" >
							Figure 2. Output from “glucosinolate” keyword search.
						</div><br>

					</ul>

					<h3><li>Browse</li></h3>
					
					<p class="text-justify">Browse page shows list of dataset tables for SuCComBase, for example. Known Arabidopsis SCC Genes, Putative Arabidopsis SCC Genes, Potential SCC Genes, SCC Homologs, Compounds and Publications. The description for every dataset was included. </p>
					<br>
					<img src="/assets/site_figure3.png" width="1200" class="pull-right gap-left img-thumbnail"> 
					<div align="center" >
						Figure 3. List of datasets and their description in Browse Tab.
					</div>
					<br>
					<p class="text-justify">When you click on any of the datasets, a list of datasets will appear as in Figure 4. User can also search the gene of interest within the dataset and download all the information of Known SCC genes in .CSV or .xls format.</p>
					<br>
					<img src="/assets/site_figure4.jpg" width="1200" class="pull-right gap-left img-thumbnail"> 
					<div align="center" >
						Figure 4. The view of Known SCC genes.
					</div>
					<br>
					<h3><li>Datasets</li></h3>
					
					<p class="text-justify">Datasets tab consists all datasets of SuCComBase. Datasets will redirect user to the target dataset pages by clicking on any of datasets. </p>
					<br>
					<img src="/assets/site_figure5.jpg" width="1200" class="pull-right gap-left img-thumbnail"> 
					<div align="center" >
						Figure 5. Dataset tab. 
					</div>
					<br>
					<h3><li>Entry Details</li></h3>
					
					<p class="text-justify">This segment showed the attributes provided when user click on any of the table from the Figure 4. The user will redirect to the SCC Gene Description as shown in Figure 6. All information including Gene name, AGI ID, Uniprot ID, Gene Function, Group of Biosynthesis, Homologs Gene, etc. Each tab of Homologs and References are clickable and contain sequence homology information in other Brassicales plant genomes. The example shows there are 25 genes are homolog to CYP71B7 and one reference found.</p>
					<br>
					<img src="/assets/site_figure6.jpg" width="1200" class="pull-right gap-left img-thumbnail"> 
					<div align="center" >
						Figure 6. SCC Gene description of ‘Cytochrome P450 71B7’.
					</div>
					<br>
				</ol>
			</div>
		</div>
	</div> 
</div>

@endsection


