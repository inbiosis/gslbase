@extends('layout.mainlayout')

@section('content')

<div class="album text-muted">
  <div class="container">
    <div class="row">
      <div class="search" style="margin-top: 40px">
        <form method="get" action="{{ url('/search') }}" class="form-horizontal">
          <input type="hidden" name="search_mode" value="simple">
          <div class="form-group">
            <div class="col-md-8">
              <div class="input-group stylish-input-group">
                <input type="text" class="form-control" placeholder="Search" id="inputEmail3" name="search_term" value="{{ Request::input('search_term') }}">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </div>
            </div>
          </div>
        </form>
      </div>


      <div class="datasets col-md-8 px-0" style="margin-top: 10px">
        <table class="table">
          <thead class="stats-header">
            <tr>
              <th class="col-md-4">Dataset</th>
              <th class="col-md-4">Entries</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Known Arabidopsis SCC Genes</td>
              <td><a href="{{ url('genes')}}">158</a></td>
            </tr>
            <tr>
              <td>Putative Arabidopsis SCC Genes</td>
              <td><a href="{{ url('putatives')}}">92</a></td>
            </tr>
            <tr>
              <td>Potential SCC Genes</td>
              <td><a href="{{ url('coexpressions')}}">778</a></td>
            </tr>
            <tr>
              <td>SCC Homologs</td>
              <td><a href="{{ url('results')}}">4070</a></td>
            </tr>
            <tr>
              <td>Compounds</td>
              <td><a href="{{ url('compounds')}}">116</a></td>
            </tr>
            <tr>
              <td>Publications</td>
              <td><a href="{{ url('references')}}">224</a></td>
            </tr>
          </tbody>
        </table>
      </div>

        <!-- hitwebcounter Code START -->
<div class="datasets col-md-8 px-0" style="margin-top: 10px">
  Visitor counter:
<a href="http://www.hitwebcounter.com" target="_blank">
<img src="http://hitwebcounter.com/counter/counter.php?page=7063157&style=0025&nbdigits=5&type=page&initCount=12" title="Home Remedies For Wrinkles" Alt="Home Remedies For Wrinkles"   border="0" >
</a><br/>
                                        <!-- hitwebcounter.com --><a href="http://www.hitwebcounter.com" title="" 
                                        target="_blank" style="font-family: ; 
                                        font-size: px; color: #; text-decoration:  ;">                                        </>
                                        </a></div>

            <div class="datasets col-md-8 px-0" style="margin-top: 10px">
              Cite us: <br><a href ="https://academic.oup.com/database/article/doi/10.1093/database/baz021/5353919" target="_blank">Sarahani Harun, Muhammad-Redha Abdullah-Zawawi, Mohd Rusman Arief A-Rahman, Nor Azlan Nor Muhammad, Zeti-Azura Mohamed-Hussein; SuCComBase: a manually curated repository of plant sulfur-containing compounds, Database, Volume 2019, baz021, 22 February 2019.</a>
            </div>




    </div>
  </div>
</div>

           


@endsection