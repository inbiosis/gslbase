<form method="get" action="{{ url('search') }}" class="form-horizontal">
  <input type="hidden" name="search_mode" value="advanced">
  {{-- First multi fields --}}
  <div class="form-group">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-2">
      <select class="form-control" id="sel2" name="field_first">
        <option>All Fields</option>
        <option>Protein Description</option>
        <option>Ontology</option>
        <option>Domain</option>
        <option>Pathway</option>
        <option>Disease</option>
      </select>
    </div>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="inputEmail3" name="search_term_first" value="{{ Request::input('search_term_first') }}">
    </div>
  </div>

  <div class="multi-field-wrapper">
    <div class="multi-fields">
      @if(!Request::has('search_terms'))
      @include('pages.input-row', ['index' => null])
      @else
      @foreach(Request::input('search_terms') as $key => $value)
      @include('pages.input-row', ['index' => $key])
      @endforeach
      @endif

    </div>

    <button type="button" class="btn btn-default add-field">Add Search Field</button>
    <button type="submit" class="btn btn-primary">Search</button>
  </div>
</form>
