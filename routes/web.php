<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('pages.welcomev2');
});

Route::get('search', 'SearchController@index');

Route::get('browse', function () {
    return view('pages.browse');
});

Route::get('about', function () {
    return view('pages.about');
});

Route::get('contact', function () {
    return view('pages.contact');
});

Route::get('download', function () {
    return view('pages.download');
});

Route::get('help', function () {
    return view('pages.help');
});

Route::get('database', function () {
    return view('pages.database');
});

Route::get('datasources', function () {
    return view('pages.datasources');
});

Route::get('glossary', function () {
    return view('pages.glossary');
});

Route::get('faqs', function () {
    return view('pages.faqs');
});
////////////////////////////////////////////////////////////
// DISPLAY
////////////////////////////////////////////////////////////

// Genes
Route::get('genes', 'GeneController@index')->name('genes.index');
Route::get('genes/{gene}', 'GeneController@show')->name('genes.show');
Route::get('datatables/genes', 'GeneController@datatablesGenes')->name('datatables.genes');

// Coexpressions
Route::get('coexpressions', 'CoexpressionController@index')->name('coexpressions.index');
Route::get('coexpressions/{coexpression}', 'CoexpressionController@show')->name('coexpressions.show');
Route::get('datatables/coexpressions', 'CoexpressionController@datatablesCoexpressions')->name('datatables.coexpressions');

// Compounds
Route::get('compounds', 'CompoundController@index')->name('compounds.index');
Route::get('compounds/{compound}', 'CompoundController@show')->name('compounds.show');
Route::get('datatables/compounds', 'CompoundController@datatablesCompounds')->name('datatables.compounds');

// Putatives
Route::get('putatives', 'PutativeController@index')->name('putatives.index');
Route::get('putatives/{putative}', 'PutativeController@show')->name('putatives.show');
Route::get('datatables/putatives', 'PutativeController@datatablesPutatives')->name('datatables.putatives');

// References
Route::get('references', 'ReferenceController@index')->name('references.index');
Route::get('references/{reference}', 'ReferenceController@show')->name('references.show');
Route::get('datatables/references', 'ReferenceController@datatablesReferences')->name('datatables.references');

// Results
Route::get('results', 'ResultController@index')->name('results.index');
Route::get('results/{result}', 'ResultController@show')->name('results.show');
Route::get('datatables/results', 'ResultController@datatablesResults')->name('datatables.results');
