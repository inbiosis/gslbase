<?php

namespace App;
use App\Result;
use Illuminate\Database\Eloquent\Model;

class Coexpression extends Model
{
  protected $table = 'coexpressions';
  protected $primaryKey = 'agi_id';
  public $incrementing = false;

  public function results()
  {
    return $this->hasMany(Result::class, 'agi_id');
  }

  public function databases()
  {
    return $this->belongsToMany(Database::class, 'coexpressions_databases', 'agi_id', 'database');
  }

  public function getDatabaseCellStyle()
  {
    if (!$this->databases->count()) {
      return '';
    } else {
      return 'color: white; background-color: hsl(220,65%, '.$this->getDatabasePercentage().'%)';
    }
  }

  public function geneontologies()
  {
    return $this->belongsToMany(Geneontology::class, 'genes_geneontologies', 'agi_id', 'go_id');
  }

}
