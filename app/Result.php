<?php

namespace App;
use App\Gene;
use App\Organism;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
	protected $table = 'results';
	protected $primaryKey = 'id';

	public function gene()
	{
		return $this->belongsTo(Gene::class, 'agi_id');
	}

	public function organism()
	{
		return $this->belongsTo(Organism::class, 'result_type_id');
	}

	  public function geneontologies()
  	{
    	return $this->belongsToMany(Geneontology::class, 'genes_geneontologies', 'agi_id', 'go_id');
  	}
}
