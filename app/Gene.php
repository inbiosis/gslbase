<?php

namespace App;
use App\Result;
use App\Reference;
use Illuminate\Database\Eloquent\Model;

class Gene extends Model
{
  protected $table = 'genes';
  protected $primaryKey = 'agi_id';
  public $incrementing = false;

  public function results()
  {
    return $this->hasMany(Result::class, 'agi_id');
  }

  public function references()
  {
    return $this->belongsToMany(Reference::class, 'genes_references', 'agi_id', 'reference_id');
  }

  public function geneontologies()
  {
    return $this->belongsToMany(Geneontology::class, 'genes_geneontologies', 'agi_id', 'go_id');
  }

}
