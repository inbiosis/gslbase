<?php

namespace App;
use App\Result;
use App\Organism;
use Illuminate\Database\Eloquent\Model;

class Compound extends Model
{
  protected $table = 'compounds';
  protected $primaryKey = 'compound_id';

  public function results()
  {
    return $this->hasMany(Result::class, 'agi_id');
  }

    public function organism()
  {
    return $this->belongsTo(Organism::class, 'result_type_id');
  }

    public function references()
  {
    return $this->belongsToMany(Reference::class, 'compounds_references', 'compound_name', 'reference_id');
  }

}
