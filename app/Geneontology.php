<?php

namespace App;
use App\Genes;
use Illuminate\Database\Eloquent\Model;

class Geneontology extends Model
{
	protected $table = 'geneontologies';
	protected $primaryKey = 'go_id';
  	public $incrementing = false;

	public function genes()
	{
		return $this->belongsToMany(Genes::class, 'genes_geneontologies', 'go_id', 'agi_id');
	}

	public function putatives()
	{
		return $this->belongsToMany(Putative::class, 'genes_geneontologies', 'go_id', 'agi_id');
	}

}
