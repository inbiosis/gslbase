<?php

namespace App\Http\Controllers;

use App\Reference;
use Illuminate\Http\Request;

class ReferenceController extends Controller
{
    /**
     * Display a listing of the resource..
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $references = Reference::query();

        if ($request->has('search_term')) {
            $term = $request->input('search_term');
            $references->where('reference_name', 'LIKE', '%'.$term.'%')
                    ->orWhere('year', 'LIKE', '%'.$term.'%')
                    ->orWhere('reference_title', 'LIKE', '%'.$term.'%')
                    ->orWhere('journal', 'LIKE', '%'.$term.'%')
                    ->orWhere('pubmed_id', 'LIKE', '%'.$term.'%');
        }

        $references = $references->get();

        return view('references.index', compact('references'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reference = Reference::find($id);

        return view('references.show', compact('reference'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
