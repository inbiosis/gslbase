<?php

namespace App\Http\Controllers;

use App\Compound;
use Illuminate\Http\Request;

class CompoundController extends Controller
{
    /**
     * Display a listing of the resource..
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $compounds = Compound::query();

        if ($request->has('search_term')) {
            $term = $request->input('search_term');
            $compounds->where('compound_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('common_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('knapsack_id', 'LIKE', '%'.$term.'%')
                        ->orWhere('formula', 'LIKE', '%'.$term.'%')
                        ->orWhere('GSL_type', 'LIKE', '%'.$term.'%'); 

        }

        if ($request->has('result_type_id')) {
            $term = $request->input('result_type_id');
            $compounds->where('result_type_id', $term);

        }

        $compounds = $compounds->get();
        return view('compounds.index', compact('compounds'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $compound = Compound::find($id);

        return view('compounds.show', compact('compound'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
