<?php

namespace App\Http\Controllers;

use App\Putative;
use Illuminate\Http\Request;

class PutativeController extends Controller
{
    /**
     * Display a listing of the resource..
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $putatives = Putative::query();

        if ($request->has('search_term')) {
            $term = $request->input('search_term');
            $putatives->where('agi_id', 'LIKE', '%'.$term.'%')
                    ->orWhere('gene_name', 'LIKE', '%'.$term.'%')
                    ->orWhere('protein_name', 'LIKE', '%'.$term.'%')
                    ->orWhere('synonym', 'LIKE', '%'.$term.'%')
                    ->orWhere('uniprot', 'LIKE', '%'.$term.'%')
                    ->orWhere('gsl_type', 'LIKE', '%'.$term.'%');
        }

        

        if ($request->has('interaction_comp_type')) {
            $term = $request->input('interaction_comp_type');
            $putatives->where('interaction_comp_type', $term);

        }

        $putatives = $putatives->get();

        return view('putatives.index', compact('putatives'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $putative = Putative::find($id);

        return view('putatives.show', compact('putative'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
