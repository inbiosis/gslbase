<?php

namespace App\Http\Controllers;

use App\Gene;
use App\Coexpression;
use App\Compound;
use App\Result;
use App\Putative;
use App\Reference;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_mode = $request->input('search_mode');

        $genes = Gene::query();
        $coexpressions = Coexpression::query();
        $compounds = Compound::query();
        $results = Result::query();
        $putatives = Putative::query();
        $references = Reference::query();

        // $results = [
        //     ["name" => "Genes", "count" => $genes->count(), "url" => "genes"],
        //     ["name" => "Putative Genes", "count" => $putatives->count(), "url" => "putatives"],
        //     ["name" => "Potential Genes", "count" => $coexpressions->count(), "url" => "coexpressions"],
        // ];

        if ($search_mode == 'advanced') {
            $protein = $this->advancedSearch($request, $proteins);

            $proteins = $proteins->paginate(100);

            $operators = $request->input('operator');
            $fields = $request->input('field');
            $search_terms = $request->input('search_terms');

            return view('proteins.index', compact('proteins', 'operators', 'fields', 'search_terms'));

        } else if ($search_mode == 'simple') {
            if ($request->has('search_term')) {
                $term = $request->input('search_term');

                $genes->where('agi_id', 'LIKE', '%'.$term.'%')
                        ->orWhere('gene_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('protein_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('synonym', 'LIKE', '%'.$term.'%')
                        ->orWhere('uniprot', 'LIKE', '%'.$term.'%')
                        ->orWhere('ec_number', 'LIKE', '%'.$term.'%')
                        ->orWhere('AraCyc', 'LIKE', '%'.$term.'%')
                        ->orWhere('KEGG', 'LIKE', '%'.$term.'%')
                        ->orWhere('group', 'LIKE', '%'.$term.'%')
                        ->orWhere('function', 'LIKE', '%'.$term.'%');

                $putatives->where('agi_id', 'LIKE', '%'.$term.'%')
                        ->orWhere('gene_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('protein_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('synonym', 'LIKE', '%'.$term.'%')
                        ->orWhere('uniprot', 'LIKE', '%'.$term.'%')
                        ->orWhere('ec_number', 'LIKE', '%'.$term.'%')
                        ->orWhere('SCC_type', 'LIKE', '%'.$term.'%')
                        ->orWhere('gsl_type', 'LIKE', '%'.$term.'%');


                $coexpressions->where('agi_id', 'LIKE', '%'.$term.'%')
                        ->orWhere('gene_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('protein_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('synonym', 'LIKE', '%'.$term.'%')
                        ->orWhere('uniprot', 'LIKE', '%'.$term.'%')
                        ->orWhere('ec_number', 'LIKE', '%'.$term.'%')
                        ->orWhere('gsl_type', 'LIKE', '%'.$term.'%');

                $results->where('agi_id', 'LIKE', '%'.$term.'%')
                        ->orWhere('gene_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('result_type_id', 'LIKE', '%'.$term.'%')
                        ->orWhere('protein_name', 'LIKE', '%'.$term.'%');

                $compounds->where('compound_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('common_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('knapsack_id', 'LIKE', '%'.$term.'%')
                        ->orWhere('formula', 'LIKE', '%'.$term.'%')
                        ->orWhere('GSL_type', 'LIKE', '%'.$term.'%');

                $references->where('reference_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('year', 'LIKE', '%'.$term.'%')
                        ->orWhere('reference_title', 'LIKE', '%'.$term.'%')
                        ->orWhere('journal', 'LIKE', '%'.$term.'%')
                        ->orWhere('pubmed_id', 'LIKE', '%'.$term.'%')
                        ->orWhere('pmc_id', 'LIKE', '%'.$term.'%');          

            }

            $results = [
                ["name" => "Genes", "count" => $genes->count(), "url" => "genes"],
                ["name" => "Putative Genes", "count" => $putatives->count(), "url" => "putatives"],
                ["name" => "Potential Genes", "count" => $coexpressions->count(), "url" => "coexpressions"],
                ["name" => "SCC Homologs", "count" => $results->count(), "url" => "results"],
                ["name" => "Compounds", "count" => $compounds->count(), "url" => "compounds"],
                ["name" => "Publications", "count" => $references->count(), "url" => "references"],


            ];

            return view('pages.search', compact('results', 'search_mode'));

        } else {
            return view('pages.search', compact('results', 'search_mode'));
        }
    }

    /**
     * do advanced search
     *
     * @return query
     */
    public function advancedSearch($request, $proteins)
    {
        if ($request->has('search_term_first')) {
            $term = $request->input('search_term_first');
            $first_field = $request->input('field_first');

            $proteins = $this->processTerm(true, null, $first_field, $term, $proteins);

            $operators = $request->input('operator');
            $fields = $request->input('field');
            $search_terms = $request->input('search_terms');

            // search all terms
            for ($i=0; $i < count($search_terms); $i++) {
                $proteins = $this->processTerm(false, $operators[$i], $fields[$i], $search_terms[$i], $proteins);
            }
        }

        return $proteins;
    }

    /**
     * process search term
     */
    public function processTerm($first, $operator, $field, $term, $proteins)
    {
        // Protein Description
        if ($first) {
            if ($field == 'Protein Description') {
                $proteins->where('description', 'LIKE', '%'.$term.'%');
            }

            // Ontology
            if ($field == 'Ontology') {
                $proteins->whereHas('geneontologies', function ($query) use ($term) {
                    $query->where('ontology', $term);
                });
            }
        } else {
            if ($field == 'Protein Description' && $operator == 'AND') {
                $proteins->where('description', 'LIKE', '%'.$term.'%');
            }
            if ($field == 'Protein Description' && $operator == 'OR') {
                $proteins->orWhere('description', 'LIKE', '%'.$term.'%');
            }
            if ($field == 'Protein Description' && $operator == 'NOT') {
                $proteins->where('description', 'NOT LIKE', '%'.$term.'%');
            }

            if ($operator == 'AND') {
                $where = 'whereHas';
            } else if ($operator == 'OR') {
                $where = 'orWhereHas';
            } else { // NOT
                $where = 'whereDoesntHave';
            }

            // Search relationship

            // Ontology
            if ($field == 'Ontology') {
                call_user_func_array([$proteins, $where], [
                    'geneontologies',
                    function ($query) use ($term) {
                        $query->where('ontology', $term);
                    }
                ]);
            }
        }

        return $proteins;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
