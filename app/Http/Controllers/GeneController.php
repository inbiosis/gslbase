<?php

namespace App\Http\Controllers;

use App\Gene;
use Illuminate\Http\Request;

class GeneController extends Controller
{
    /**
     * Display a listing of the resource..
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $genes = Gene::query();

        if ($request->has('search_term')) {
            $term = $request->input('search_term');
            $genes->where('agi_id', 'LIKE', '%'.$term.'%')
                    ->orWhere('gene_name', 'LIKE', '%'.$term.'%')
                    ->orWhere('protein_name', 'LIKE', '%'.$term.'%')
                    ->orWhere('synonym', 'LIKE', '%'.$term.'%')
                    ->orWhere('uniprot', 'LIKE', '%'.$term.'%')
                    ->orWhere('function', 'LIKE', '%'.$term.'%');
        }

        $genes = $genes->get();

        return view('genes.index', compact('genes'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gene = Gene::find($id);

        return view('genes.show', compact('gene'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
