<?php

namespace App;
use App\Gene;
use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
  protected $table = 'references';
  protected $primaryKey = 'reference_id';

  public function genes()
  {
    return $this->belongsToMany(Gene::class, 'genes_references', 'reference_id', 'agi_id');
  }

    public function compounds()
  {
    return $this->belongsToMany(Compound::class, 'compounds_references', 'reference_id', 'compound_name');
  }

  
  // ACCESSORS
  public function getPubmedIdAttribute($value)
  {
    return !empty($value) ? $value : 'N/A';
  }

  public function getDoiAttribute($value)
  {
    return !empty($value) ? $value : 'N/A';
  }

  public function getPmcIdAttribute($value)
  {
    return !empty($value) ? $value : 'N/A';
  }

}
