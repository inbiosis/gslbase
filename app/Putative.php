<?php

namespace App;
use App\Type;
use Illuminate\Database\Eloquent\Model;

class Putative extends Model
{
  protected $table = 'putatives';
  protected $primaryKey = 'agi_id';
  public $incrementing = false;

  public function type()
  {
    return $this->belongsTo(Type::class, 'interaction_comp_type');
  }

  public function geneontologies()
  {
    return $this->belongsToMany(Geneontology::class, 'genes_geneontologies', 'agi_id', 'go_id');
  }

}
