<?php

namespace App;
use App\Putative;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
	protected $table = 'putatives_type';
	protected $primaryKey = 'interaction_comp_type';

	public function putatives()
	{
		return $this->hasMany(Putative::class, 'interaction_comp_type');
	}

}
