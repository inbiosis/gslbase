<?php

namespace App;
use App\Coexpression;
use Illuminate\Database\Eloquent\Model;

class Database extends Model
{
	protected $table = 'databases';

	public function coexpressions()
	{
		return $this->belongsToMany(Coexpression::class, 'coexpressions_databases', 'database', 'agi_id');
	}

}
