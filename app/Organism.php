<?php

namespace App;
use App\Result;
use Illuminate\Database\Eloquent\Model;

class Organism extends Model
{
	protected $table = 'organisms';
	protected $primaryKey = 'result_type_id';

	public function results()
	{
		return $this->hasMany(Result::class, 'result_type_id');
	}
}
